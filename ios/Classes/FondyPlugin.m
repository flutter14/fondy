#import "FondyPlugin.h"
#if __has_include(<fondy/fondy-Swift.h>)
#import <fondy/fondy-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "fondy-Swift.h"
#endif

@implementation FondyPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFondyPlugin registerWithRegistrar:registrar];
}
@end
